/*
 * File: GripperClient.h
 * Author: Andoni Aguirrezabal
 */

#ifndef CARRTGRIPPERCONTROL_H
#define CARRTGRIPPERCONTROL_H

#include <boost/function.hpp>
#include <string>

#include "CARRTGripperDefines.h"
#include "GripperClients/GripperControlClient.h"
#include "GripperClients/GripperSensorClient.h"

namespace CARRT {

    /*! \brief CARRT Gripper Main Control Class */
    class CARRTGripperController {
    public:
        CARRTGripperController();
        ~CARRTGripperController();

    // Information Methods
    public:
        /*! \brief Get the hardware version of the CARRT Gripper Control Box
         * 
         * @return a FLOAT representing the version of the CARRT Gripper 
         * Control Box's hardware
         */
        float getHardwareVersion(void) {
            return hardwareVersion;
        };
        /*! \brief Get the software version of the CARRT Gripper Control Box
         * 
         * @return a FLOAT representing the version of the CARRT Gripper 
         * Control Box's software
         */
        float getSoftwareVersion(void) {
            return softwareVersion;
        };
        /*! \brief Returns the current state of the Gripper
         * 
         * @return a GRIPPERSTATUS datatype representing the state of the state
         * of the Gripper Connection
         */
        GRIPPERSTATUS getGripperStatus(void);

    // Configuration Methods
    public:
        /*! \brief Creates a callback to the given function on arrival of 
         * new sensor data
         * 
         * @param [in] function  The function to be called when new sensor
         * data arrives
         */
        void attachSensorCallback(boost::function<void (void)> function);
        /*! \brief Connects to the Gripper Controller
         *
         * @return true if the connection to both the control server and sensor
         * server was successful, false if either connection failed
         */
        bool connect(void);
        /*! \brief Disconnects from the Gripper Controller
         * 
         * @return true if the disconnection from the control server and sensor
         * server was successful, false if either disconnection failed
         */
        bool disconnect(void);
        /*! \brief Sets the connection parameters to the Gripper Controller
         *
         * @param [in] address  The IP Address or Hostname of the Grippper
         * Controller server
         * @param [in] controlPort  The Port Number for the Control Commands
         * [DEFAULT = 2320]
         * @param [in] sensorPort  The Port Number for the Sensor Information
         * [DEFAULT = 2325]
         */
        void initialize(std::string address, int controlPort, int sensorPort);
        /*! \brief Test function
         * 
         * @param [in] ???
         */
        void test(std::string& teststring);
        /*! \brief New test function
         * 
         */
        void testneue(void);
        
    // Gripper Control Methods
    public:
        /*! \brief Begin Moving
         * 
         * @return a GRIPPERERROR datatype containing the results of the command
         */
        GRIPPERERROR beginMoving(void);
        /*! \brief Set ADC Limit
         *
         * @return a GRIPPERERROR datatype containing the results of the command
         */
        GRIPPERERROR setADCLimit(int adcChannel, int value);
        /*! \brief Set Speed
         * 
         * @param [in] newSpeed  The desired speed value in integer form ranging
         *  from 0 to 255
         * @return a GRIPPERERROR datatype containing the results of the command
         */        
        GRIPPERERROR setSpeed(int newSpeed);
        /*! \brief Reverse Direction
         *
         * @return a GRIPPERERROR datatype containing the results of the command
         */
        GRIPPERERROR reverseDirection(void);
        /*! \brief Set Direction
         * 
         * @param [in] GRIPPERDIR  The desired direction
         * @return a GRIPPERERROR datatype containing the results of the command
         */        
        GRIPPERERROR setDirection(GRIPPERDIR newDirection);
        /*! \brief Stop
         * 
         * @return a GRIPPERERROR datatype containing the results of the command
         */
        GRIPPERERROR stop(void);

    // Data Control Methods
    private:
        void updateAdcData(int* newData);
        
    // Client Instances
    private:
        GripperControlClient controlClient;
        GripperSensorClient sensorClient;

    // Control Variables
    private:
        boost::function<void (void)> dataCallback;
        
    // Configuration Variables
    private:
        std::string serverAddress;
        int serverControlPort;
        int serverSensorPort;

        //Hardware and Software versions of the gripper
        float hardwareVersion;
        float softwareVersion;

    // Status Variables
    private:
        /*! \brief Returns the connection status of the Gripper Control Client
         * 
         * @return boolean, true if both the control client and sensor client 
         * are actively connected to the Gripper Controller
         */
        bool isConnected(void) {
            return (controlClient.isConnected() && sensorClient.isConnected());
        };
        bool isInitialized;
    };
}

#endif  /* CARRTGRIPPERCONTROL_H */

