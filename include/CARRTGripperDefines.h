/* 
 * File:   CARRTGripperDefines.h
 * Author: Andoni Aguirrezabal
 */

#ifndef CARRTGRIPPERDEFINES_H
#define	CARRTGRIPPERDEFINES_H

namespace CARRT {

    /*! \brief Gripper Status
     *
     * Typedef for the various gripper status codes
     */
    typedef enum {
        /*! \brief Gripper Uninitialized
         * 
         * The gripper control instance has not been configured to 
         * connect to a gripper. 
         */
        GRIPPER_UNINTIALIZED,
        /*! \brief Gripper Disconnected
         *
         * The gripper control instance is not currently connected to a
         * gripper control box
         */
        GRIPPER_DISCONNECTED,
        /*! \brief Gripper Connected
         *
         * The gripper control instance is currently connected to the
         * gripper control box
         */
        GRIPPER_CONNECTED
    } GRIPPERSTATUS;

    /*! \brief Gripper Direction
     *
     * Direction of the Gripper
     */
    typedef enum {
        /*! \brief Direction Closing
         */
        GDIR_CLOSE,
        /*! \brief Direction Opening
         */
        GDIR_OPEN
    } GRIPPERDIR;
    
    /*! \brief Communication Method
     * 
     * Typedef for the various gripper communication methods
     */
    typedef enum {
        GRIPPER_COMMETHOD_TCP,
        GRIPPER_COMMETHOD_BT,
        GRIPPER_COMMETHOD_SERIAL
    } GRIPPERCOMMETHODS;
    
    /*! \brief Gripper Error Codes
     * 
     * Typedef for the various return codes from gripper functions
     */
    typedef enum {
        /*! \brief Unspecified Error
         */
        GERR_UNKNOWN,
        /*! \brief Command OK, No Error
         */
        GERR_OK,
        /*! \brief Gripper Not Connected
         */
        GERR_NOTCONNECTED = 100,
        /*! \brief Argument Outside Acceptable Range
         */
        GERR_INVALIDVALUE = 200
    } GRIPPERERROR;
}

#endif /* CARRTGRIPPERDEFINES_H */

