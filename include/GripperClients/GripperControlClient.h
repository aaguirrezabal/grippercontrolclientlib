/*
 * File:   GripperControlClient.h
 * Author: Andoni Aguirrezabal
 */

#ifndef GRIPPERCONTROLCLIENT_H
#define GRIPPERCONTROLCLIENT_H

#include "SockStream.h"

namespace CARRT {

    /*! \brief Gripper Control Client
     *
     * This Class Represents the TCP Connection to the Gripper
     * that exchanges control information
     */
    class GripperControlClient {
    public:
        GripperControlClient();
        ~GripperControlClient();

    public:
        bool connect(const std::string&, const int&);
        bool disconnect(void);
        bool isConnected(void) {
            return tcpSocket.connected();
        }

    public:
        bool senddata(const std::string& data);

    private:
        client_tcpsocket tcpSocket;
    };

}

#endif  /* GRIPPERCONTROLCLIENT_H */

