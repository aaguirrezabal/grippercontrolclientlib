/* 
 * File:   GripperSensorClient.h
 * Author: Andoni Aguirrezabal
 */

#ifndef GRIPPERSENSORCLIENT_H
#define	GRIPPERSENSORCLIENT_H

#include <boost/thread.hpp>
#include <boost/date_time.hpp>

#include "SockStream.h"

namespace CARRT {

    /*! \brief Gripper Sensor Client
     *
     * This Class Represents the TCP Connection to the Gripper
     * that exchanges sensor information
     */
    class GripperSensorClient {
    public:
        GripperSensorClient();
        ~GripperSensorClient();

    // Configuration Methods
    public:
        void attachSensorCallback(boost::function<void (int*)> function);
        
    // Action
    public:
        bool connect(const std::string&, const int&); // Connect the Sensor 
                                                      // Client
        bool disconnect(void); // Disconnect the Sensor Client
        void sensorLoop(void); // Enters Main Sensor Loop
    
    // Information
    public:
        bool isConnected(void) {
            return tcpSocket.connected();
        }
        
    // Configuration Variables
    private:
        boost::function<void (int*)> dataCallback; // Callback to Main Data
        
        boost::thread* sensorThread; // Pointer to Thread
        client_tcpsocket tcpSocket;  // TCP Socket
    };
}

#endif  /* GRIPPERSENSORCLIENT_H */

