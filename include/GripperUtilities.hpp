/* 
 * File:   GripperUtilities.hpp
 * Author: Andoni Aguirrezabal
 *
 * Created on December 17, 2014, 3:43 PM
 */

#ifndef GRIPPERUTILITIES_H
#define	GRIPPERUTILITIES_H

#include <sstream>

std::string IntToString(int number) {
    // Create Output String Stream
    std::ostringstream oss;
    oss << number;
    return oss.str();
}

int CharArrayToInt(const char& inputArray) {
    return 0;
}

#endif	/* GRIPPERUTILITIES_H */

