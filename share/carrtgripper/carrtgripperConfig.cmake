# ===============================================================================================
#  carrtgripper CMake configuration file
#
#             ** File generated automatically, do not modify **
#
#  Usage from an external project:
#    In your CMakeLists.txt, add these lines:
#
#    FIND_PACKAGE(carrtgripper REQUIRED )
#    TARGET_LINK_LIBRARIES(MY_TARGET_NAME )
#
#    This file will define the following variables:
#      - carrtgripper_LIBS          : The list of libraries to links against.
#      - carrtgripper_LIB_DIR       : The directory where lib files are. Calling LINK_DIRECTORIES
#                                     with this path is NOT needed.
#      - carrtgripper_VERSION       : The  version of this PROJECT_NAME build. Example: "1.2.0"
#      - carrtgripper_VERSION_MAJOR : Major version part of VERSION. Example: "1"
#      - carrtgripper_VERSION_MINOR : Minor version part of VERSION. Example: "2"
#      - carrtgripper_VERSION_PATCH : Patch version part of VERSION. Example: "0"
#
# ===============================================================================================
INCLUDE_DIRECTORIES("/home/andoni/Documents/CARRT Stuff/Gripper/GripperControlClientLib/include")
SET(carrtgripper_INCLUDE_DIRS "/home/andoni/Documents/CARRT Stuff/Gripper/GripperControlClientLib/include")

LINK_DIRECTORIES("/home/andoni/Documents/CARRT Stuff/Gripper/GripperControlClientLib/dist/Debug/GNU-Linux-x86")
SET(carrtgripper_LIB_DIR "/home/andoni/Documents/CARRT Stuff/Gripper/GripperControlClientLib/dist/Debug/GNU-Linux-x86")

SET(carrtgripper_LIBS GripperControlClient)

SET(carrtgripper_FOUND          1     )
SET(carrtgripper_VERSION        1.0.0 )
SET(carrtgripper_VERSION_MAJOR  1     )
SET(carrtgripper_VERSION_MINOR  0     )
SET(carrtgripper_VERSION_PATCH  0     )
