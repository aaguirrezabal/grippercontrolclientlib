/*
 * File:   CARRTGripperControl.cpp
 * Author: Andoni Aguirrezabal
 * Description: This file exposes the public interface for using the
 *              CARRT Gripper Client Library
 */

#include "CARRTGripperControl.h"

#include "GripperUtilities.hpp"

using namespace CARRT;

// Constructor
CARRTGripperController::CARRTGripperController() {
    // Gripper Status
    isInitialized = false;

    // INIT Server Information
    serverAddress = "";
    serverControlPort = -1;
    serverSensorPort = -1;
    
    // INIT Server Hardware Information
    hardwareVersion = 0.1;
    softwareVersion = 0.1;
    
    // sensorClient.attachSensorCallback(&updateAdcData);
}

// Destructor
CARRTGripperController::~CARRTGripperController() {
    // NOP
}

// Outside Callback Function
void CARRTGripperController::attachSensorCallback(
        boost::function<void (void)> function) {
    dataCallback = function;
}

bool CARRTGripperController::connect(void) {
    if (!isInitialized) {
        return false;
    } else {
        if (getGripperStatus() == GRIPPER_CONNECTED) {
            return true;
        } else {
            if ((controlClient.connect(serverAddress, serverControlPort)) && 
                (sensorClient.connect(serverAddress, serverSensorPort))) {
                return true;
            } else {
                return false;
            }
        }   
    }
}

bool CARRTGripperController::disconnect(void) {
    if(controlClient.disconnect() && sensorClient.disconnect()) {
        return true;
    } else {
        return false;
    }
}

GRIPPERSTATUS CARRTGripperController::getGripperStatus(void) {
    if(!isInitialized) {
        return GRIPPER_UNINTIALIZED;
    } else {
        if (isConnected()) {
            return GRIPPER_CONNECTED;
        } else {
            return GRIPPER_DISCONNECTED;
        }
    }
}

void CARRTGripperController::initialize(std::string address,
        int controlPort = 2320, int sensorPort = 2325)
{
    if(!isConnected()) {
        disconnect();
    }

    serverAddress = address;
    serverControlPort = controlPort;
    serverSensorPort = sensorPort;
    isInitialized = true;
}

GRIPPERERROR CARRTGripperController::beginMoving(void) {
    if(isConnected()) {
        std::string gripperCmd = std::string("@") + "\n";
        if(controlClient.senddata(gripperCmd)) {
            return GERR_OK;
        } else {
            return GERR_UNKNOWN;
        }
    } else {
        return GERR_NOTCONNECTED;
    }
}

GRIPPERERROR CARRTGripperController::reverseDirection(void) {
    if(isConnected()) {
        std::string gripperCmd = std::string("#") + "\n";
        if(controlClient.senddata(gripperCmd)) {
            return GERR_OK;
        } else {
            return GERR_UNKNOWN;
        }
    } else {
        return GERR_NOTCONNECTED;
    }
}

GRIPPERERROR CARRTGripperController::setADCLimit(int adcChannel, int value) {
    if((adcChannel > 7) || (adcChannel < 0) || (value > 1024) || (value < 0)) {
        return GERR_INVALIDVALUE;
    } else {
        if(isConnected()) {
            std::string gripperCmd = std::string("L") + IntToString(adcChannel)
                         + ":" + IntToString(value) + "\n";
            if(controlClient.senddata(gripperCmd)) {
                return GERR_OK;
            } else {
                return GERR_UNKNOWN;
            }
        } else {
            return GERR_NOTCONNECTED;
        }
    }
}

GRIPPERERROR CARRTGripperController::setSpeed(int newSpeed) {
    if((newSpeed > 255) || (newSpeed < 0)) {
        return GERR_INVALIDVALUE;
    } else {
        if(isConnected()) {
            std::string gripperCmd = std::string("$") + IntToString(newSpeed)
                         + "\n";
            if(controlClient.senddata(gripperCmd)) {
                return GERR_OK;
            } else {
                return GERR_UNKNOWN;
            }
        } else {
            return GERR_NOTCONNECTED;
        }
    }
}

GRIPPERERROR CARRTGripperController::stop(void) {
    if(isConnected()) {
        std::string gripperCmd = std::string("!") + "\n";
        if(controlClient.senddata(gripperCmd)) {
            return GERR_OK;
        } else {
            return GERR_UNKNOWN;
        }
    } else {
        return GERR_NOTCONNECTED;
    }
}

void CARRTGripperController::test(std::string& teststring) {
    controlClient.senddata(teststring);
}

//Prototype for Callback Code
void CARRTGripperController::testneue(void) {
    if(dataCallback) {
        dataCallback();
    } else {
        std::cout << "\033[0;31m[CRITICAL]\033[0m Callback not set!\n";
    }
}

void CARRTGripperController::updateAdcData(int* newData) {
    for(int i = 0; i < 10; i++) {
        
    }
}
