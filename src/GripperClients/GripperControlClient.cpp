/* 
 * File:   GripperControlClient.cpp
 * Author: Andoni Aguirrezabal
 */

#include "GripperClients/GripperControlClient.h"

#include <iostream>

using namespace CARRT;

// Constructor
GripperControlClient::GripperControlClient() {
    // NOP
}

// Destructor
GripperControlClient::~GripperControlClient() {
    tcpSocket.close();
}

bool GripperControlClient::connect(const std::string& address, 
        const int& port)
{
    // Open Connection to Client
    if(!tcpSocket.connected()) {
        tcpSocket.open(address.c_str(), port);
        if(tcpSocket.connected()) {
            return true;
        } else {
            return false;
        } 
    } else {
        return true; // We are already connected
    }
}

bool GripperControlClient::disconnect(void) {
    tcpSocket.close();
    return (!tcpSocket.connected());
}

bool GripperControlClient::senddata(const std::string& data) {
    if (tcpSocket.connected()) {
        if (data.length() > 0) {
            std::cout << tcpSocket.write(data.c_str(), data.length());
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

