/* 
 * File:   GripperSensorClient.cpp
 * Author: Andoni Aguirrezabal
 */

#include <iostream>

#include "GripperClients/GripperSensorClient.h"
#include "CARRTGripperControl.h"

using namespace CARRT;

// Constructor
GripperSensorClient::GripperSensorClient() {
    // NOP
}

// Destructor
GripperSensorClient::~GripperSensorClient() {
    tcpSocket.close();
}

// Control Class Callback
void GripperSensorClient::attachSensorCallback(
        boost::function<void (int*)> function) {
    dataCallback = function;
}

bool GripperSensorClient::connect(const std::string& address, 
        const int& port)
{
    // Open Connection to Client
    if(!tcpSocket.connected()) {
        tcpSocket.open(address.c_str(), port);
        if(tcpSocket.connected()) {
            sensorThread = new boost::thread(
                    &GripperSensorClient::sensorLoop, this);
            return true;
        } else {
            return false;
        }
    } else {
        return true; // We are already connected
    }
}

bool GripperSensorClient::disconnect(void) {
    tcpSocket.close();
    return (!tcpSocket.connected());
}

void GripperSensorClient::sensorLoop(void) {
    int adcDataBuffer[10] = {0};
    char inputBuffer[51] = {0};

    while(tcpSocket.connected()) {
        // Read Command Message from Gripper
        tcpSocket.read(inputBuffer, 51);
        if (inputBuffer[50] != '\n') {
            // Eat the rest of the data
            std::cout << "[CRITICAL] Buffer Error\n";
        } else {
            inputBuffer[50] = '\0';
            sscanf(inputBuffer, "%d:%d:%d:%d:%d:%d:%d:%d:%d:%d:",
                                &adcDataBuffer[0],
                                &adcDataBuffer[1],
                                &adcDataBuffer[2],
                                &adcDataBuffer[3],
                                &adcDataBuffer[4],
                                &adcDataBuffer[5],
                                &adcDataBuffer[6],
                                &adcDataBuffer[7],
                                &adcDataBuffer[8],
                                &adcDataBuffer[9]);

            if(dataCallback) {
                dataCallback(&adcDataBuffer[0]);
            }
        }
    }
}

